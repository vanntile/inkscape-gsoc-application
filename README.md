# Inkscape GSoC 2019

## Table of contents
- [Preamble](#preamble)
- [Interacting with the Inkscape world](#interacting-with-the-inkscape-world)
- [Part I. JavaScript Polyfills](#part-i-javascript-polyfill)
    - [Mesh gradients](#mesh-gradients)
        - [Rendering model](#rendering-model)
    - [Mesh gradient polyfill](#mesh-gradient-polyfill)
    - [Hatches](#hatches)
- [Roadmap status](#roadmap-status)
- [Part II. GTK Work](#part-ii-gtk-work)
    - [Paint servers dialog](#paint-servers-dialog)
    - [Hatch UIX](#hatch-uix)
- [Learning from GSoC](#learning-from-gsoc)
- [Roadmap status](#roadmap-status)
- [Links and references](#links-and-references)

The links to the four merge request to my work can be found in each section, or
in the [links and references](#links-and-references).

## Preamble

Hello, everyone, I am Valentin Ionita and I am proud to be the [2019 Inkscape GSoC][1]
chosen student. My project, presented in the following document, is based on
improving paint server support for the vector graphics application known as
[Inkscape][2]. Inkscape is different from other graphics software in the wild by
being a completely open-sourced vector editing software that is compliant with
the [W3C SVG Standard][3]. My contribution is due to several different reasons.

For the first part, I have developed JavaScript polyfills (as my initial
[proposal][4] stated). The need for these appeared because the SVG 2 2016 [draft][5]
mentioned development for extensive polyfill functionality, including both
mesh gradients and hatches, but they were later dropped, as is their status in
the current [recommendation][6]. In the meantime, they were introduced in
Inkscape, and, while the mesh gradients have been heavily relied as a new
functionality, the modern browsers have dropped their implementation creating a
need for JavaScript polyfills that help rendering Inkscape files with these
types of paint servers in the browser.

For the second part, my work has been revolving around improving the support of
hatches in Inkscape that, while being render-able, they are overlooked because
there is not a sufficient user interface to allow the usual, non-technical,
users to interact with them. My work tried to add small UI elements available to
other paint servers (i.e. patterns) to the hatches and introduce a new dialog in
Inkscape with the purpose of previewing and setting the paint server. The
potential of this new dialog is being fit for extension and, in the future,
being a main tool for managing, previewing and setting all types of paint
servers (including the above-mentioned hatches and patterns, but also all types
of gradients) from resource files coming with Inkscape but also user input.

## Interacting with the Inkscape world

I have been using Inkscape for a few years now and, at the start of 2019, I have
started involving myself with the developer community. My mentor has been
[Tavmjong Bah][7], a top [contributor][8] and core team member of the project.
The guidance I received from Tav, both from a vision and from a technical
point of view, has been priceless, teaching me how I should attack problems for
a coding project, but, especially, for an open-source one. What I definitely
have to mention is that the whole dev community in Inkscape has been supportive
from the start to the end. Helping with packaging, merging, code quality and
explanation about the architecture was only a slice of what I've received in the
[Inkscape Rocket Chat][9].

## Part I. JavaScript Polyfills

Initially having overlooked the opportunity of my project's topic, my proposal
was looking to mainly add JavaScript support to the current Inkscape paint
servers. As I have previously found out, before the project began, there
already existed an implementation for a mesh gradient polyfill. However, this
hasn't the extensibility that was in the spec or the C++ implementation in
Inkscape.

### Mesh gradients

The public MR for this functionality can be found [here][19].

Directly from the SVG proposal, a mesh gradient's purpose is:

> Added to allow complex shadings. This is needed, for example, in creating
> life-like drawings or in interpolating data points on a two-dimensional grid.

This special type of gradient is based on a four stops color-specification
concept called `mesh patch`. Having more patches in a series can create a
`mesh row`. A `mesh gradient` is, obviously, made out of at least one mesh row.
What makes special out of this structure is that we can use it to "compress"
data by eliminating some of the color stops and inferring it from neighboring
patches. An example of this specification is below

```xml
<meshgradient x="50" y="50" id="example"> <!-- x, y used for initial point in first patch. -->
  <meshrow> <!-- No attributes, used only to define begin/end of row. -->
	<meshpatch>
	  <stop path="c  25,-25  75, 25  100,0" stop-color="lightblue" />
	  <stop path="c  25, 25 -25, 75  0,100" stop-color="purple" />
	  <stop path="c -25, 25 -75,-25 -100,0" stop-color="red" />
	  <stop path="c -25,-25, 25,-75"        stop-color="purple" /> <!-- Last point not needed (closed path). -->
	</meshpatch>
	<meshpatch>
	  <stop path="c  25,-25  75, 25  100,0" /> <!-- stop-color from previous patch. -->
	  <stop path="c  25, 25 -25, 75  0,100" stop-color="lightblue" />
	  <stop path="c -25, 25 -75,-25"        stop-color="purple" /> <!-- Last point not needed (closed path). -->
	  <!-- Last path (left side) taken from right side of previous path (with points reversed). -->
	</meshpatch>
  </meshrow>
  <meshrow> <!-- New row. -->
	<meshpatch>
	  <!-- First path (top side) taken from bottom path of patch above. -->
	  <stop path="c  25, 25 -25, 75  0,100" /> <!-- stop-color from patch above. -->
	  <stop path="c -25, 25 -75,-25 -100,0" stop-color="purple" />
	  <stop path="c -25,-25, 25,-75"        stop-color="lightblue" /> <!-- Last point not needed (closed path). -->
	</meshpatch>
	<meshpatch>
	  <!-- First path (top side) taken from bottom path of patch above (with points reversed). -->
	  <stop path="c  25, 25 -25, 75  0,100" /> <!-- stop-color from patch above. -->
	  <stop path="c -25, 25 -75,-25"        stop-color="lightblue" /> <!-- Last point not needed (closed path). -->
	  <!-- Last path (left side) taken from right side of previous path (with points reversed). -->
	</meshpatch>
  </meshrow>
</meshgradient>
```

As their definition says, the mesh gradient is purposed to be a versatile
drawing tool. You can see below some of the graphics that can be achieved with
mesh gradients (also rendered in the browser).

![mesh gradient examples][10]

PS: these are some of the actual tests I used for my code, from Tav.

#### Rendering model

Interpolation in a 2D space is an expansion of 1D interpolation – to interpolate
a value in a point between four corners (the values being the color channels)
is achieved by interpolating two parallel axis, then projecting the point on the
two parallels and interpolating the resulting points. Below we have represented
the nearest-neighbor, linear and bicubic interpolation.

![interpolation models][11]

Attribution: [Cmglee CC BY-SA 4.0][12]

What's necessary to point out is that a patch is not rectangular. It can be any
four sided shape, each side determined by a Bezier curve (the standard model of
representing a curve in the SVG standard). In the figure below, I hope you can
see more clearly that interpolating values needs to be projected on the curves
determining the fixed points.

![patch interpolation][13]

[Bicubic interpolation][14] is an interpolation method that increases the
quality of the end-result, having less artifacts, solving issues like overly
sharp transitions, but introducing overshooting or haloing. These latter effect
can be diminished by a few methods and has to be taken into account while
writing a numerical algorithm that outputs into a graphical environment.

### Mesh gradient polyfill

My work on mesh gradients polyfills has had the following objectives:
- improve the code quality
- switching to modern JavaScript syntax without losing performance
- increasing performance, if possible
- adding gradients on strokes (previously only existing on fill)
- adding bicubic interpolation alongside the existing bilinear

While the first goals have proved to be tasks that introduce effort and
expertise in the web, the last task introduced a theoretical necessity. I
started by brushing off my university numerical method skills and read
different resources (from Richard Burden's Numerical Analysis to Wikipedia
articles and cairo's implementation). The test file for a complex rendering of a
bicubic rendering is [pepper.svg][15] and you can see the final, intended result
in the third version in the image below.

Having several misunderstandings on how four control color stops are transposed
in the sixteen points a bicubic interpolation needs to work, I tried writing my
own algorithm from scratch. The result was... psychedelic, as you can see in
**version 1** in the image below. You can see that the structure of the pepper
is kept, while the color are neither continuous or predictable. After further
analysis of the problem, I understood that it was the result of making a
double linear interpolation, with uneven steps (something similar, but not quite
bilinear).

The second version is the result of following the Wikipedia computation of the
algorithm, but inversing the indices of the derivatives, similar to rotating
the axes 90 degrees for some patches. Looking totally different from the first
rendering, we can see that each path has the correct rendering, but the total
structure is clearly ignored. I have learned that in a complex mathematical
script, a beginner mistake can push development a few days behind, because
debugging won't help. There were no bugs. 😅 The general approach taken to
compute the bicubic meshes are interpolating, for each hatch patch, a grid of
8x8 smaller patches bicubically and further interpolating its pixels bilinearily.

![pepper rendering][16]

As a final observation, the performance of rendering the pepper (a heavily
computed file) wasn't affected much by using bicubic interpolation rather than
bilinear. It averaged `10.4 seconds`, an increase on the pepper of only `0.8 s`
from the version without bicubic (about 9% increase in time).

### Hatches

The public MR for this functionality can be found [here][20].

Directly from the SVG proposal, a hatch's purpose:

> They were added to allow the kinds of patterns required for mapping, engraving,
> etc. where continuous lines are needed.

Hatches are specialised paint servers that repeat one or several `hatchpaths` by
a fixed formula based on the element's attributes - x, y, rotate and pitch. Each
hatchpath is repeated "infinitely" on one axis and on `pitch` distance along the
other, rotated by the `rotate` attribute.

> (x + m * pitch * cos(rotate), y + m * pitch * sin(rotate), for each integer m

Totally different than the mesh polyfill, where we computed every pixel to a
canvas that was used as a base image for a pattern mask, creating hatches was
made from the ground up, by creating a pattern for each hatchpath and
calculating the SVG Bezier path command arguments and translating them to
simulate an infinite y-axis hatchpath. A much simpler approach is taken when the
hatchpath has no data attribute, being a simple line. Because a pattern is not
infinite, another issue appeared – how large does our initial pattern need to be
to account for any roation angle and still fill the whole bounding box of our
object? Understanding exactly the minimum amount of overshoot is important as
every new datapoint in the hatch increases the pattern rendering enourmously.


```javascript
// the diagonal of the object's bounding box is the basis for our calculations
const hatchDiag = Math.ceil(Math.sqrt(
  bbox.width * bbox.width + bbox.height * bbox.height
));

// the computed datapoints are determined by having a box larger than the
// bounding box by (diagonal - length) / 2 + offset, offset being equal to the
// pitch on one side and determined by the hatcpath data height on the other
let patternWidth = bbox.width - bbox.width % pitch;
const xPositions = generatePositions(patternWidth, hatchDiag, x, pitch);

let patternHeight = bbox.height - bbox.height % yOffset;
const yPositions = generatePositions(
  patternHeight, hatchDiag, y, yOffset
);
```

Below you can see some test hatches that vary in complexity and attributes
tested: simple to complex hatch paths, different pitches, rotations, transforms,
reference hatches and rendering errors.

![hatch tests renders][17]

## Part II. GTK Work

For the second part, I have worked on improving the actual Inkscape interface
and functionality concerning paint servers. This was completely different for me,
passing from a scripting language, JavaScript, to a programming one, C++, and,
especially, the GTK framework to manage the interface. The Inkscape stack
includes gtkmm, a `GTK+` interfaces for C++, glibmm, giomm and a few others.

The steep learning curve I had with managing GNOME widgets was due to two
reasons - the concept of signals involving C++ memory management and the actual
fact that Gtk code examples in C++ are difficult to find. Official documentation
is somehow poor, self-referencing and it's better to look it up in Python.
Happily, the best source of understanding how a complex behavior using gtk
widgets was actually Inkscape. So that's exactly how I started to write code,
after I understood the environment - by making a demo dialog.

### Paint servers dialog

The public MR for this functionality can be found [here][21].

Starting as the **hatchDialog** branch, this Dialog was Tav's recommendation of
me learning how to work with the stack, that, step by step, turned into a
fully-fledged, functional dialog. The purpose of this dialog is to manage paint
servers (i.e. patterns and hatches – maybe gradients in the future). You can use
the dialog to set the fill or stroke of the selected objects, just as the swatch
dialog. Its main point is to preview default paint servers in `share/paint`. It
can be reached by going to `Objects > Paint Servers`.

Difficulties in implementing the micro-functionalities of the dialog were
various: signal connections for loading servers, updating the current document
store, clicking on an item, recursively scanning a source document for servers,
showing them in an `IconView`.

```c++
// Extract paints from the current file
load_document(desktop->getDocument());

// Extract out paints from files in share/paint.
for (auto &path : get_filenames(Inkscape::IO::Resource::PAINT, { ".svg" })) {
    SPDocument * document = SPDocument::createNewDoc(path.c_str(), FALSE);
    Glib::ustring document_title = Glib::ustring(document->getRoot()->title());

    load_document(document);
}
```

This brand can someday in the future become the complete paint-servers preview
and management dialog, allowing setting selected object properties, editing
paint servers and showing the current document's ones.

### Hatch UIX

The public MR for this functionality can be found [here][22].

A most interesting final task was to improve the UI & UX of hatches in the
Inkscape interface. This implied I have added editing handles (also known as
knot handlers) for hatches that appear when you have an element with a hatch and
use a specific set of tools, for example, the node editing tool. This
functionality already existed in patterns and, as it fitted the paint server
architecture, it was about time to introduce them. Using the handles, you could
rotate, scale and translate the paint server.

![hatch knot handlers][18]

A current bug that I couldn't fix was that, after using the handles to add a
`matrixTransform` to the hatch, on secondary edits, the hatch wouldn't update.
Specifically, the hatch wouldn't be rerendered on transforms, even if its
attribute is modified - the first update is due to a secondary hatch with a
transform that is added and referencing the origin paint server. This bug has
been detected in other situations - on copying an element with a hatch from a
document to another, the hatch doesn't get rendered. This was due to the above
mentioned bug, but also due to the fact that the XML tree doesn't get copied
(fact that I fixed in this series of modifications).

A few more improvements were to fix consistency problems with paint servers -
now you can see in the status bar if you have selected an element with a hatch
fill or stroke - and a few similar fixes.

## Learning from GSoC

The GSoC experience was something I had been waiting for a whole year, since the
first time I have heard of a program like this. Moreover, when I found Inkscape
to be one of the organizations, a program I had been using for such a long time,
I decided to be one of the mentees for sure. Now that the program is done,
I can tell that I have experienced and learned a few things I can't give up:

- performance in JavaScript without DOM-manipulation is difficult
- numerical algorithms can be combined to obtain viable implementations
- GTK+ is a complex, not complicated, framework, close to maturity
- documentation of popular frameworks is necessary but not sufficient
- patience is required when exploring solving a problem
- on a decentralized project, involvement in secondary features or difficult to
implement ones is the duty of the independent developers, not the main team
- asking the community can be the greatest source of truth

For this experience, I appreciate everyone from the Inkscape team, be them in
the dev, ux or the vectors teams. Thank you!

## Roadmap status

- [DONE] Check [W3C's Editor's Draft][draft] of the corresponding features
- [DONE] Document existing polyfills (mesh on fill)
- [DONE] How Inkscape can use polyfills? (Mesh on strokes, hatching)
- [DONE] Assess the status of SVG2 features drafts and real implementations
between browsers and frameworks
- [DONE] Week 0: Prototype approach for future implementations (Advice needed)
- [CANC] Week 0: List major mesh features implemented in Inkscape
- [DONE] Weeks 1-2: EcmaScript6+ polyfill rewrite
- [DONE] Week 3: Mesh on stroke implementation (expand current mesh polyfill)
(**JS**)
- [DONE] Week 4: Mesh bicubic implementation (**JS**)
- [DONE] Weeks 5-6: Hatch and hatch path implementation (**JS**)
- [DONE] Weeks 7-9: Paint server gtk widget for GUI use (rewrite or integration)
- [DONE] Weeks 10-12: Codebase integrations and documenting (essential,
non-blocking)
- [Stretch goal] Weeks 12-13: GUI Hatch editor

-----
## Links and references

The four branches that hold my modifications are the following:
- [Mesh gradients JS polyfill][19]
- [Hatch paint server JS polyfill][20]
- [Paint servers dialog][21]
- [Hatch control][22]


[1]: https://summerofcode.withgoogle.com/organizations/5311282658410496/#5365695322259456
[2]: https://inkscape.org/
[3]: https://www.w3.org/TR/SVG/Overview.html
[4]: ./Application.md
[5]: https://www.w3.org/TR/2016/CR-SVG2-20160915/Overview.html
[6]: https://www.w3.org/TR/SVG/pservers.html
[7]: http://tavmjong.free.fr/
[8]: https://gitlab.com/inkscape/inkscape/-/graphs/master
[9]: https://chat.inkscape.org/channel/inkscape_user
[10]: ./graphics/mesh_gradient_examples.jpg
[11]: https://upload.wikimedia.org/wikipedia/commons/9/90/Comparison_of_1D_and_2D_interpolation.svg
[12]: https://creativecommons.org/licenses/by-sa/4.0
[13]: ./graphics/patch_interpolation.png
[14]: https://en.wikipedia.org/wiki/Bicubic_interpolation
[15]: ./graphics/pepper.svg
[16]: ./graphics/pepper_rendered.jpg
[17]: ./graphics/hatches_rendered.png
[18]: ./graphics/hatches_knot_handlers.png
[19]: https://gitlab.com/inkscape/inkscape/merge_requests/746
[20]: https://gitlab.com/inkscape/inkscape/merge_requests/754
[21]: https://gitlab.com/inkscape/inkscape/merge_requests/789
[22]: https://gitlab.com/inkscape/inkscape/merge_requests/838

[draft]: https://svgwg.org/svg2-draft/
